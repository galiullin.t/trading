import com.ib.client.Order;

/**
 * Created by timur on 2017-07-12.
 */
public class OrderSimulator {
    double money;

    public OrderSimulator(double money) {
        this.money = money;
    }


    public double placeOrder(int tickerId, String type, int numPositions, double stockPrice){
        if (type.equals("SELL")){
            this.money = this.money + stockPrice*numPositions;
        }

        if (type.equals("BUY")){
            this.money = this.money - stockPrice*numPositions;
        }

        return this.money;
    }
}
