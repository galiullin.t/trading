/**
 * Created by timur on 2017-06-29.
 */
public class Bar {
    String date;
    double open;
    double high;
    double low;
    double close;
    String size;
    int volume;
    int count;

    public Bar(String size, String date, double open,
               double high, double low, double close) {
        this.size = size;
        this.date = date;
        this.open = open;
        this.high = high;
        this.low = low;
        this.close = close;
        this.date = date;
    }
}
