import com.ib.client.EClient;
import com.ib.client.Order;

/**
 * Created by timur on 2017-07-02.
 */
public class OrderManager {
    EClient client;
    EWrapperImpl wrapper;
    int orderId;
    Order order;
    static int freeId = 100;

    public OrderManager(EWrapperImpl wrapper) {
        this.wrapper = wrapper;
        this.freeId = this.wrapper.getCurrentOrderId() + 101;
    }

    public void placeOrder(int tickerId, String type, int numPositions, double trailingPercent){
        this.orderId = this.freeId + 1;
        this.order = new Order();
        this.order.account("DU629078");

        if (type.equals("TRAIL STOP SELL")){
            this.order.action("SELL");
            this.order.orderType("TRAIL");
            this.order.trailingPercent(trailingPercent);
            this.order.totalQuantity(numPositions);
            this.freeId++;
        }

        if (type.equals("MARKET BUY")){
            this.order.action("BUY");
            this.order.orderType("MKT");
            this.order.totalQuantity(numPositions);
            this.freeId++;
        }

        this.wrapper.getClient().placeOrder(this.orderId, this.wrapper.getContract(tickerId), this.order);
    }
}
