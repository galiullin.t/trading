import com.ib.client.*;
import com.sun.org.apache.xpath.internal.operations.Bool;
import com.tictactec.ta.lib.Core;
import com.tictactec.ta.lib.MAType;
import com.tictactec.ta.lib.MInteger;
import com.tictactec.ta.lib.RetCode;

import java.io.IOException;
import java.util.Vector;

/**
 * Created by timur on 2017-06-10.
 */

public class Main {
    static boolean backtestMode = true;
    static int contractNumber = 10;


    public static void main(String[] args) throws InterruptedException {
        String[][] tickers;

        int[] taParameters = {
                14, // ADX period
                14,  // Fast K period 1
                70, // Fast K period 2
                3,  // Slow K period
                1   // Slow D period
        };

        double[] buySignals = {
                20.0, // ADX signal
                80.0, // FastK 7 signal
                50.0  // FastK 70 signal
        };

        String barSize = "1 min";

        ExcelReader excelReader = new ExcelReader();
        String[][] contractData;

            try {
                excelReader.setInputFile("../Stocks.xls");
                tickers = excelReader.readStockData();

                contractData = new String[tickers.length][];

                int h = 0;
                for (int i = 0; i < tickers.length; i++) {
                    if (tickers[i] != null) {
                        String[] lineStock = {tickers[i][0], tickers[i][1], "USD"};
                        contractData[h] = lineStock;
                        h++;
                    }
                }


            Contract[] contracts = new Contract[contractNumber];

            for (int i = 0; i < contractNumber; i++) {
                String[] contractLine = contractData[i];
                Contract contract = new Contract();
                contract.secType("STK");

                if (contractLine[0] != null && contractLine[1] != null && contractLine[2] != null) {
                    contract.symbol(contractLine[0]);
                    contract.exchange(contractLine[1]);
                    contract.currency(contractLine[2]);
                    contracts[i] = contract;
                }
            }


            EWrapperImpl wrapper = new EWrapperImpl(barSize, contracts, buySignals, taParameters, backtestMode);

            final EClientSocket m_client = wrapper.getClient();
            final EReaderSignal m_signal = wrapper.getSignal();
            m_client.eConnect("127.0.0.1", 7497, 0);

            final EReader reader = new EReader(m_client, m_signal);
            reader.start();
            new Thread() {
                public void run() {
                    while (m_client.isConnected()) {
                        m_signal.waitForSignal();
                        try {
                            reader.processMsgs();
                        } catch (Exception e) {
                            System.out.println("Exception: " + e.getMessage());
                            e.printStackTrace();
                        }
                    }
                }
            }.start();

            Thread.sleep(1000);


            for (int j = 0; j < contracts.length; j++) {
                if(backtestMode){
                    wrapper.getClient().reqHistoricalData(j, contracts[j],
                            "",  // End Date/Time
                            "10 d",                // Duration
                            "1 min",              // Bar size
                            "TRADES",             // What to show
                            1,                    // useRTH
                            1,                    // dateFormat
                            null);
                }
                else {
                    wrapper.getClient().reqRealTimeBars(j, contracts[j], 5, "TRADES", true, null);
                }
            }


            System.out.println("Data requested. ");

//            if(backtestMode){
//                Thread.sleep(10000);
//                m_client.eDisconnect();
//            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}