/**
 * Created by timur on 2017-07-06.
 */


import java.io.File;
import java.io.IOException;

import jxl.Cell;
import jxl.CellType;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class ExcelReader {

    private String inputFile;

    public void setInputFile(String inputFile) {
        this.inputFile = inputFile;
    }

    public String[][] readStockData() throws IOException {
        File inputWorkbook = new File(inputFile);
        Workbook w;
        String[][] tickers;
        try {
            w = Workbook.getWorkbook(inputWorkbook);
            Sheet sheet = w.getSheet(0);

            tickers = new String[sheet.getRows()][2];

            for (int i = 0; i < sheet.getRows(); i++) {
                Cell tickerCell = sheet.getCell(0, i);
                Cell exchangeCell = sheet.getCell(1, i);
                tickers[i][0] = tickerCell.getContents();

                if(exchangeCell.getContents().equals("Nasdaq")){
                    tickers[i][1] = "ISLAND";
                }

                if(exchangeCell.getContents().equals("NYSE") || exchangeCell.getContents().equals("NYSE Arca")){
                    tickers[i][1] = "SMART";
                }
            }

            return tickers;
        } catch (BiffException e) {
            e.printStackTrace();
        }
        return null;
    }

}