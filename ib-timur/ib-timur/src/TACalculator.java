import com.ib.client.Contract;
import com.ib.client.EClientSocket;
import com.ib.client.EReaderSignal;
import com.ib.client.Order;
import com.sun.org.apache.xpath.internal.operations.Bool;
import com.tictactec.ta.lib.Core;
import com.tictactec.ta.lib.MAType;
import com.tictactec.ta.lib.RetCode;
import com.tictactec.ta.lib.MInteger;
import com.sun.tools.javac.util.ArrayUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.*;


/**
 * Created by timur on 2017-06-29.
 */
public class TACalculator {
    int tickerId;
    Core core;
    EWrapperImpl wrapper;
    Contract contract;
    Logger logger;
    Logger backtestLogger;
    double[] backtestTrailingPrice;

    String barSize;

    Bar barArray[] = new Bar[10000];
    Bar historicalBarArray[] = new Bar[10000];
    int currentHistoricalSize = 0;
    Boolean historicalCalled = Boolean.FALSE;


    double highArray[] = new double[10000];
    double lowArray[] = new double[10000];
    double openArray[] = new double[10000];
    double closeArray[] = new double[10000];

    String currentDate;
    double currentLow;
    double currentHigh;

    double currentAdx;
    double currentSlowKOne;
    double currentSlowKTwo;
    double prevSlowKOne;
    double prevSlowKTwo;

    double currentOpen;
    double currentClose;
    int currentSize = 0;

    boolean backtestMode;
    OrderSimulator orderSimulator;

    String[] timeFractions = {"00", "05", "10", "15", "20", "25", "30", "35", "40", "45", "50", "55"};
//    HashMap<String, Integer> barSizes = {"1min" : 12, "2min" : 24, "3min" : 36, "5min" : 60};

    int lineSize = 0;

    public TACalculator(int tickerId, EWrapperImpl wrapper, String barSize, boolean backtestMode) {
        Core c = new Core();
        this.core = c;
        this.logger = new Logger("./orderLog.txt");
        this.backtestLogger = new Logger("./orderLogTest.txt");

        this.wrapper = wrapper;
        this.contract = wrapper.getContract(tickerId);
        this.barSize = barSize;
        this.tickerId = tickerId;
        this.backtestMode = backtestMode;

        if (backtestMode) {
            this.orderSimulator = new OrderSimulator(1000000);
            this.backtestTrailingPrice = new double[100];
        }
    }

    public void setWrapper(EWrapperImpl wrapper) {
        this.wrapper = wrapper;
    }

    public void pushBar(Bar bar, int[] parameters, double[] signals) {

        if (bar.size == "5s") {
            String fraction = bar.date.length() > 2 ? bar.date.substring(bar.date.length() - 2) : bar.date;

            if (Arrays.asList(this.timeFractions).indexOf(fraction) == this.currentSize) {
                if (fraction.equals("00") && this.currentSize == 0) {
                    this.currentOpen = bar.open;
                    this.currentLow = bar.low;
                    this.currentHigh = bar.high;
                    this.currentDate = bar.date;
                }

                if (fraction.equals("55") && this.currentSize == 11) {
                    this.currentClose = bar.close;
                }

                if (this.currentLow > bar.low) {
                    this.currentLow = bar.low;
                }

                if (this.currentHigh < bar.high) {
                    this.currentHigh = bar.high;
                }

                this.currentSize++;

                if (fraction.equals("55") && this.currentSize == 12) {
                    this.highArray[lineSize] = this.currentHigh;
                    this.lowArray[lineSize] = this.currentLow;
                    this.openArray[lineSize] = this.currentOpen;
                    this.closeArray[lineSize] = this.currentClose;

                    this.barArray[lineSize] = new Bar("1 min", this.currentDate, this.currentOpen, this.currentHigh, this.currentLow, this.currentClose);

                    this.currentHigh = 0;
                    this.currentLow = 0;
                    this.currentOpen = 0;
                    this.currentClose = 0;
                    this.currentSize = 0;

                    System.out.println("\n");
                    System.out.println("Time: " + this.currentDate);
                    System.out.println("Open: " + this.openArray[lineSize] + ", High: " + this.highArray[lineSize] +
                            ", Low: " + this.lowArray[lineSize] + ", Close: " + this.closeArray[lineSize]);

                    this.lineSize++;

                    try {
                        this.updateIndicators(parameters, signals);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        } else {
            this.barArray[lineSize] = bar;
            this.highArray[lineSize] = bar.high;
            this.lowArray[lineSize] = bar.low;
            this.openArray[lineSize] = bar.open;
            this.closeArray[lineSize] = bar.close;
            this.lineSize++;
        }
    }

    public void updateIndicators(int[] parameters, double[] signals) throws InterruptedException {

        int adxPeriod = parameters[0];
        int fastKPeriodOne = parameters[1];
        int fastKPeriodTwo = parameters[2];
        int slowKPeriod = parameters[3];
        int slowDPeriod = parameters[4];

        if (!backtestMode && (this.lineSize > 0 && this.lineSize < 100 && !this.historicalCalled)) {
            loadBars(this.barArray[0].date);
        }

        this.currentAdx = this.adx(adxPeriod);

        this.prevSlowKOne = this.currentSlowKOne;
        this.prevSlowKTwo = this.currentSlowKTwo;

        this.currentSlowKOne = this.stochastic(fastKPeriodOne, slowKPeriod, slowDPeriod);
        this.currentSlowKTwo = this.stochastic(fastKPeriodTwo, slowKPeriod, slowDPeriod);

        StringBuilder line = new StringBuilder();

        line.append("Time ");
        line.append(this.barArray[this.lineSize - 1].date);
        line.append(" Period #");
        line.append(this.lineSize);
        line.append(" ADX = ");
        line.append(currentAdx);
        line.append(" slow K = ");
        line.append(currentSlowKOne);

        line.append(" slow K 70 = ");
        line.append(currentSlowKTwo);
        System.out.println(line.toString());

        if (this.backtestMode) {

            if(this.barArray[this.lineSize - 1].close <= backtestTrailingPrice[this.tickerId]*0.95){
                double moneyLeft = this.orderSimulator.placeOrder(this.tickerId, "SELL", 100, this.barArray[this.lineSize - 1].close);
            }
    }



            if (currentAdx < signals[0] && prevSlowKOne < signals[1] && currentSlowKOne >= signals[1] && currentSlowKTwo > signals[2]) {
            double[] indicators = {currentAdx, prevSlowKOne, currentSlowKOne, currentSlowKTwo};

            if (this.backtestMode) {
                if(backtestTrailingPrice[this.tickerId] == 0.0){
                    double moneyLeft = this.orderSimulator.placeOrder(this.tickerId, "BUY", 100, this.barArray[this.lineSize - 1].close);
                    backtestTrailingPrice[this.tickerId] = this.barArray[this.lineSize - 1].close;
                    this.backtestLogger.printSignal(this.barArray[this.lineSize - 1].date, this.contract, indicators, moneyLeft);
                }
            } else {
                this.logger.printSignal(this.currentDate, this.contract, indicators, 0);
                this.wrapper.getOrderManager().placeOrder(this.tickerId, "MARKET BUY", 100, 0);
                this.wrapper.getOrderManager().placeOrder(this.tickerId, "TRAIL STOP SELL", 100, 0.5);
            }
        }
    }

    public void loadBars(String date) {
        //CALCULATE NUMBER OF CANDLES
        this.wrapper.getClient().reqHistoricalData(this.tickerId, this.contract,
                date,  // End Date/Time
                "4500 S",                // Duration
                "1 min",              // Bar size
                "TRADES",             // What to show
                1,                    // useRTH
                1,                    // dateFormat
                null);

        System.out.println("HISTORICAL LOADED");
        this.historicalCalled = Boolean.TRUE;
    }

    public void updateHistoricalBars(Bar bar) {
        this.historicalBarArray[this.currentHistoricalSize] = bar;
        this.currentHistoricalSize++;
        String incrDate;
        Bar resultArray[] = new Bar[1000];
        double[] resultHighArray = new double[1000];
        double[] resultLowArray = new double[1000];
        double[] resultOpenArray = new double[1000];
        double[] resultCloseArray = new double[1000];


        try {
            DateFormat sdf = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(sdf.parse(bar.date));
            calendar.add(Calendar.MINUTE, 1);  // number of days to add
            incrDate = sdf.format(calendar.getTime());

            if (incrDate.equals(barArray[0].date)) {
                int i = 0;
                int j = 0;

                while (i < this.currentHistoricalSize) {
                    resultArray[i] = this.historicalBarArray[i];
                    resultHighArray[i] = this.historicalBarArray[i].high;
                    resultLowArray[i] = this.historicalBarArray[i].low;
                    resultOpenArray[i] = this.historicalBarArray[i].open;
                    resultCloseArray[i] = this.historicalBarArray[i].close;
                    i++;
                }
                while (j < this.lineSize) {
                    resultArray[i + j] = this.barArray[j];
                    resultHighArray[i + j] = this.highArray[j];
                    resultLowArray[i + j] = this.lowArray[j];
                    resultOpenArray[i + j] = this.openArray[j];
                    resultCloseArray[i + j] = this.closeArray[j];
                    j++;
                }
                this.barArray = resultArray;
                this.highArray = resultHighArray;
                this.lowArray = resultLowArray;
                this.openArray = resultOpenArray;
                this.closeArray = resultCloseArray;

                this.lineSize += this.currentHistoricalSize;
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    private double adx(int period) {
        double[] outputADX = new double[this.lineSize];
        MInteger outBegIdxADX = new MInteger();
        MInteger outNbElementADX = new MInteger();

        RetCode adx = core.adx(0, this.lineSize - 1, this.highArray, this.lowArray, this.closeArray, 14,
                outBegIdxADX, outNbElementADX, outputADX);

        if (adx == RetCode.Success) {
            if (outNbElementADX.value > 0) {
                return outputADX[outNbElementADX.value - 1];
            }
        }
        return 0.0;
    }

    private double stochastic(int fastKPeriod, int slowKPeriod, int slowDPeriod) {
        MInteger outBegIdxStoch = new MInteger();
        MInteger outNbElementStoch = new MInteger();
        double[] outSlowK = new double[this.lineSize];
        double[] outSlowD = new double[this.lineSize];
        double outputStoch = 0.0;
        double[] roundedStoch;


        RetCode stoch = core.stoch(0, this.lineSize - 1, this.highArray, this.lowArray, this.closeArray,
                fastKPeriod, slowKPeriod, MAType.Sma, slowDPeriod, MAType.Sma,
                outBegIdxStoch, outNbElementStoch, outSlowK, outSlowD);

        roundedStoch = new double[outSlowK.length];

        if (stoch == RetCode.Success) {

            for (int k = 0; k < outSlowK.length; k++) {
                roundedStoch[k] = Math.round(outSlowK[k] * 10000.0) / 10000.0;
            }

            if (outNbElementStoch.value > 0) {
                outputStoch = roundedStoch[outNbElementStoch.value - 1];
            }
        }
        return outputStoch;
    }
}
