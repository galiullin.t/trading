import com.ib.client.Contract;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.IOException;

/**
 * Created by timur on 2017-07-06.
 */
public class Logger {
    String fileName;

    public Logger(String fileName) {
        this.fileName = fileName;
    }

    public void printSignal(String date, Contract contract, double[] indicators, double moneyLeft) {
        StringBuilder line = new StringBuilder();

        FileWriter fileWriter = null;

        try {
            fileWriter = new FileWriter(this.fileName, true);
        } catch (IOException e) {
            e.printStackTrace();
        }
        BufferedWriter out = new BufferedWriter(fileWriter);

        line.append(date + "\n");
        line.append("Signal on stock ");
        line.append(contract.symbol());
        line.append(" --- ADX = ");
        line.append(indicators[0]);

        line.append("  |  previous slow K 14 = ");
        line.append(indicators[1]);

        line.append("  |  slow K 14 = ");
        line.append(indicators[2]);

        line.append("  |  slow K 70 = ");
        line.append(indicators[3]);

        if(moneyLeft != 0){
            line.append("\n");
            line.append("Money left: " + moneyLeft);
        }
        line.append("\n");

        try {
            out.write(line.toString() + "\n\n");
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
